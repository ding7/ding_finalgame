﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class posionDamage : MonoBehaviour
{

    public GameObject player;
    public PlayerHealth playerHealth;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        Debug.Log(playerHealth.currentHealth);
    }

    void Update()
    {
        if(playerHealth.currentHealth == 0)
        {
            Destroy(this.gameObject);
        }
    }


    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerHealth.TakeDamage(10);
        }
    }
}