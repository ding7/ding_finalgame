﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickup : MonoBehaviour {

    public PlayerHealth playerHealth;
    public PlayerMovement playerMove;
    public PlayerAttack playerAttack;



    void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Health"))
        {
            GameObject thePlayer = GameObject.Find("Player");
            PlayerHealth playerHealth = thePlayer.GetComponent<PlayerHealth>();

            playerHealth.currentHealth += 25;

            playerHealth.healthSlider.value = playerHealth.currentHealth;

            Destroy(other.gameObject);
        }

        if (other.CompareTag("Power"))
        {
            GameObject thePlayer = GameObject.Find("Player");
            PlayerMovement playerMove = thePlayer.GetComponent<PlayerMovement>();
            PlayerAttack playerAttack = thePlayer.GetComponent<PlayerAttack>();

            playerMove.speed = 10f;
            playerAttack.damage = 50;

            Destroy(other.gameObject);
        }


    }
}
