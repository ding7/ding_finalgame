﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {

    public int damage = 20;
    public bool kickActive = false;

    void Start()
    {

        PlayerMovement playerMovement = GetComponent<PlayerMovement>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Enemy" && kickActive == true)
        {


            Vector3 direction = col.transform.position - transform.position;

            direction = direction.normalized;
            float knockBackPower = 3f;
            col.gameObject.GetComponent<Rigidbody>().velocity += direction * knockBackPower;

            EnemyHealth enemyHealth = col.gameObject.GetComponent<EnemyHealth>();

            damage = 20;

            enemyHealth.TakeDamage(damage);

        }
    }
}
