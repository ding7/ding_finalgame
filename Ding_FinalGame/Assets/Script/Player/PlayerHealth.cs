﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;

    Animator anim;
    bool isDead;
    bool damaged;

    PlayerMovement playerMovement;
    public float restartDelay = 5f;

    void Start()
    {
        playerMovement = GetComponent<PlayerMovement>();
        currentHealth = startingHealth;

        anim = GetComponent<Animator>();
    }


    void Update()
    {
       

        if (isDead == true)
        {
            restartDelay -= Time.deltaTime;
            if(restartDelay <= 0)
            {
                RestartLevel();
                restartDelay = 5f;
            }
        }

    }


    public void TakeDamage(int amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;



        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }


    }


    void Death()
    {
        isDead = true;
        anim.SetTrigger("Dead");

        playerMovement.enabled = false;

        

    }


    public void RestartLevel()
    {
        SceneManager.LoadScene("Playable");
    }
}