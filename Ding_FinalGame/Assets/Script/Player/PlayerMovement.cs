﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public bool isGrounded;
    public float speed = 5f;
    public float speedTimer = 5f;
    public float coolDown = 2f;

    PlayerAttack kickAttack;
    public float kickTimer = 0.5f;

    public float jumpForce = 300;
    public float rotSpeed = 6f;
    Rigidbody rb;
    Animator anim;



	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

        kickAttack = GetComponent<PlayerAttack>();
        print(kickAttack);
        isGrounded = true;

        speed = 6f;
	}

    // Update is called once per frame
    void Update()
    {

        var z = Input.GetAxis("Vertical") * speed;
        var y = Input.GetAxis("Horizontal") * rotSpeed;
        z *= Time.deltaTime;

        
        transform.Translate(0, 0, z);
        transform.Rotate(0, y, 0);


        if (kickAttack.kickActive == true)
        {
            kickTimer -= Time.deltaTime;
            if (kickTimer <= 0)
            {
                kickAttack.kickActive = false;
                kickTimer = 0.5f;
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {

            SpeedUp();

        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpForce = 500;
            rb.AddForce(0, jumpForce, 0);
            anim.SetTrigger("isJumping");
            isGrounded = false;

        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            anim.SetTrigger("punch");
            kickAttack.kickActive = true;
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            anim.SetTrigger("kick");
            kickAttack.kickActive = true;

        }
        
        

        if (Input.GetKey(KeyCode.W))
        {

            rotSpeed = 4f;
            
            anim.SetBool("isWalking", true);
            anim.SetBool("isIdle", false);
            
        }
        else if (Input.GetKey(KeyCode.S))
        {

            rotSpeed = 2f;
            
            anim.SetBool("isWalking", false);
            anim.SetBool("walkBackwards", true);
            anim.SetBool("isIdle", false);
            
        }
        
        else
        {
            anim.SetBool("walkBackwards", false);
            anim.SetBool("isWalking", false);
            anim.SetBool("isIdle", true);
           
        }
        
    }







    void OnCollisionEnter()
    {
        isGrounded = true;
        
    }

    void SpeedUp()
    {
        if (speedTimer >= 0)
        {
            speedTimer -= Time.deltaTime;
            print(speedTimer);
            speed = 15f;
        }
        else
        {
            speed = 6f;
            speedTimer = 5f;
        }
    }
}
