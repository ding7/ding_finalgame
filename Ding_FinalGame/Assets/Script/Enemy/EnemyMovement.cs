﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    Animator anim;
    Transform player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;

    EnemyAttack enemyAttack;
    UnityEngine.AI.NavMeshAgent nav;

    public float distance;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerHealth = player.GetComponent<PlayerHealth>();
        enemyHealth = GetComponent<EnemyHealth>();
        enemyAttack = GetComponent<EnemyAttack>();
        
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        distance = Vector3.Distance(player.transform.position, transform.position);

        if (enemyAttack.playerInRange)
        {
            nav.enabled = false;
            anim.SetBool("isWalking", false);
            anim.SetBool("IsWalk", false);
        }
        else
        { 
            if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0 && distance < 10f)
            {
                nav.enabled = true;
                nav.SetDestination(player.position);
                anim.SetBool("isWalking", true);
                anim.SetBool("IsWalk", true);
            }
            
            else
            {
                nav.enabled = false;
                anim.SetBool("isWalking", false);
                anim.SetBool("IsWalk", false);

            }

        }

    }
}
