﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;



    Animator anim;


    bool isDead;
  


    void Start()
    {
        anim = GetComponent<Animator>();


        currentHealth = startingHealth;
    }



    public void TakeDamage(int amount)
    {
        if (isDead)
            return;

        currentHealth -= amount;
        healthSlider.value = currentHealth;

        if (currentHealth <= 0)
        {
            Death();
        }
    }


    void Death()
    {
        isDead = true;

      

        anim.SetTrigger("IsDead");

    }


}